from pymongo import MongoClient
from zipcodes import *
import datetime
import cStringIO
import s3interface

def datetimetest():
    now = datetime.datetime.now()
    year = str(now.year)
    month = str(now.month)
    day = str(now.day)
    cur_day = {'year': year, 'month': month, 'day': day}
    print('cur_day : ', cur_day)
    return cur_day


# return the current time stapm in year:month:day:hr:min:sec format
def get_current_timestamp():
    now = datetime.datetime.now()
    year = str(now.year)

    month = str(now.month)
    if now.month < 10:
        month = '0' + month

    day = str(now.day)
    if now.day < 10:
        day = '0' + day

    hour = str(now.hour)
    if now.hour == 0:
        hour = '00'
    elif now.hour < 10:
        hour = '0' + hour

    minute = str(now.minute)
    if now.minute == 0:
        minute = '00'
    elif now.minute < 10:
        minute = '0' + minute

    second = str(now.second)
    if now.second == 0:
        second = '00'
    elif now.second < 10:
        second = '0' + second

    current_time_stamp = year + month + day + hour + minute + second
    # print ('Current time stamp: ', current_time_stamp)
    return current_time_stamp


# given the current year:month:day calculate the 7 days back date
# assuming each month is 30 days to keep it simple
# cur_day in this JSON format: year:year, month:month, day:day
def get_prev_week_year_month_day(cur_day):
    year = cur_day['year']
    month = cur_day['month']
    day = cur_day['day']

    prev_year = 0
    prev_month = 0
    prev_day = 0

    try:
        if (int(day) > 7):
            if (int(day) - 7) >= 10:
                prev_day = str(int(day) - 7)
            else:
                prev_day = '0' + str(int(day) - 7)

            prev_month = month
            prev_year = year
        else:
            prev_day = str(30 - 7 + int(day))
            if int(month) == 1:
                prev_month = '12'
                prev_year = str(int(year) - 1)
            else:
                if int(month) - 1 >= 10:
                    prev_month = str(int(month) - 1)
                else:
                    prev_month = '0' + str(int(month) - 1)

                prev_year = year
    except:
        year == 0 or month == 0 or day == 0
        print ('not able to get previous week year, month, and date')

    if prev_year != 0 and prev_month != 0 and prev_day != 0:
        prev_week = {'year': prev_year, 'month': prev_month, 'day': prev_day}
    else:
        print ('No valid previous week data was found')
        prev_week = {'year': 0, 'month': 0, 'day': 0}

    return prev_week


# given the ending timestamp, find out the beginning week's timestamp
# timestamp format is yyyymmddhhmmss
def get_prev_week_timestamp(cur_timestamp):
    cur_year = cur_timestamp[0:4]
    cur_month = cur_timestamp[4:6]
    cur_day = cur_timestamp[6:8]
    cur_jason = {'year': cur_year, 'month': cur_month, 'day': cur_day}
    # print ('    current json date is ', cur_jason)

    beg_json = get_prev_week_year_month_day(cur_jason)
    # print ('    begin json date is ', beg_json)

    beg_timestamp = beg_json['year'] + beg_json['month'] + beg_json['day'] + '000000'
    # print ('begin time stamp is ', beg_timestamp)

    return beg_timestamp




# this outputs studio by week, and then 1 bed by week, etc
def rent_summary_json_by_bed_week_s3(city, start_day, num_week, outfn, s3bucket):
    print('open file for writing : ', outfn)
    # f = open(outfn, 'w')
    # f.write('{"weekly":' + str(num_week) + ',')

    output = cStringIO.StringIO()
    output.write('{"weekly":' + str(num_week) + ',')


    # these are mongodb config - DO NOT CHANGE
    MyMongoClient = MongoClient("mongodb://tester:tester@ds051843.mongolab.com:51843/craiglist")
    MyMongoDB = MyMongoClient.craiglist
    RentalCollection = MyMongoDB.rental_new

    zips = supported_cities[city]
    print ('target zip list is ', zips)

    # the start date should like year:month:date
    start_time_stamp = start_day['year'] + start_day['month'] + start_day['day'] + '235959'
    begin_timestamp = 0

    # NOTE: end_timestamp is newer than begin_timestamp, we're going back in time
    for bed in range(0, 6, 1):

        for i in range(0, num_week, 1):

            if i == 0:
                end_timestamp = start_time_stamp
                begin_timestamp = get_prev_week_timestamp(end_timestamp)
            else:
                end_timestamp = begin_timestamp
                begin_timestamp = get_prev_week_timestamp(end_timestamp)

            avg_size = 0
            avg_price = 0
            sum_size = 0
            sum_rent = 0
            num_rentals_size = 0
            num_rentals_price = 0

            print ('Search in window: ', begin_timestamp, ' and ', end_timestamp)

            if bed < 5:
                all_matching_rentals = RentalCollection.find(
                    {"Zipcode": {"$in": zips}, "Bedrooms": str(bed),
                     "Timestamp": {"$lt": end_timestamp, "$gt": begin_timestamp}})
            else:
                all_matching_rentals = RentalCollection.find(
                    {"Zipcode": {"$in": zips}, "Bedrooms": {"$gte": str(bed)},
                     "Timestamp": {"$lt": end_timestamp, "$gt": begin_timestamp}})

            if all_matching_rentals.count() == 0:
                print ('no record was found, next loop')
            else:

                for matching_rental in all_matching_rentals:

                    # if we see a bad sample data then we skip both price and size calculation
                    isgooddata = True

                    p = matching_rental.get("Price")
                    # we have bad data in the database where p = 0 as an int instead of a string this might be the case
                    # where we didn't find a valid price number
                    if p != 0:
                        try:
                            # only accept rent less than $10,000/m - there are some invalid ads about selling houses
                            # instead of renting, currently no good way to filter those
                            if int(p) > 10000:
                                # print ('found big p: ', p)
                                isgooddata = False

                            if p.isdigit() and isgooddata:
                                # if int(p) > 5000:
                                #    print('large p found : ', p);
                                num_rentals_price += 1
                                sum_rent += int(p)
                        except ValueError:
                            print('bad price: ', p)

                    s = matching_rental.get("Size")
                    # we have bad data in the database where s = 0 as an int instead of a string this might be the case
                    # where we didn't find a valid size number
                    if s != 0 and isgooddata:
                        try:
                            if s.isdigit():
                                num_rentals_size += 1
                                sum_size += int(s)
                        except ValueError:
                            print ('bad size ', s)

                if (num_rentals_size > 0):
                    avg_size = int(sum_size / num_rentals_size)
                # else:
                #    avg_size = 0

                if (num_rentals_price > 0):
                    avg_price = int(sum_rent / num_rentals_price)
                # else:
                #    avg_price = 0

                print('Working on number of beds? ', bed)
                print('average size: ', avg_size)
                print('average price: ', avg_price)
                print('total good size count : ', num_rentals_size)
                print('total good price count: ', num_rentals_price)

            # f.write('"' + begin_timestamp + '-' + end_timestamp + '":')
            # f.write(str(avg_price) + ',')

            start_year = begin_timestamp[0:4]
            start_month = begin_timestamp[4:6]
            start_day = begin_timestamp[6:8]

            end_year = end_timestamp[0:4]
            end_month = end_timestamp[4:6]
            end_day = end_timestamp[6:8]

            # studio don't have the 0br tag - we use these dates as x-axis label
            if bed > 0:
                print >> output, '"' + str(bed) + 'br/' + start_year + '/' + start_month + '/' + start_day + '-' + end_year + '/' + end_month + '/' + end_day + '":'
            else:
                print >> output, '"' + start_year + '/' + start_month + '/' + start_day + '-' + end_year + '/' + end_month + '/' + end_day + '":'
            print >> output, str(avg_price) + ','

    # f.seek(-1, 1)
    # f.write('}')
    # f.close()
    f1 = output.getvalue()[:-2]
    f2 = f1 + '}'
    output.close()
    s3interface.upload_file_to_s3(f2, outfn, s3bucket)
    print('done putting file on s3: ', outfn)



# this function is similar to write_rent_summary_json_by_week_beds except it's by number of bed first, then by week
# studio week1
# studio week2
# etc
# 1 br week1
# 1 br week2
# etc
# all other params should be the same
def write_rent_summary_json_by_bed_week(city, start_day, num_week, outfn):
    print('open file for writing : ', outfn)
    f = open(outfn, 'w')
    f.write('{"weekly":' + str(num_week) + ',')

    # these are mongodb config - DO NOT CHANGE
    MyMongoClient = MongoClient("mongodb://tester:tester@ds051843.mongolab.com:51843/craiglist")
    MyMongoDB = MyMongoClient.craiglist
    RentalCollection = MyMongoDB.rental_new

    zips = supported_cities[city]
    print ('target zip list is ', zips)

    # the start date should like year:month:date
    start_time_stamp = start_day['year'] + start_day['month'] + start_day['day'] + '235959'
    begin_timestamp = 0

    # NOTE: end_timestamp is newer than begin_timestamp, we're going back in time
    for bed in range(0, 6, 1):

        for i in range(0, num_week, 1):

            if i == 0:
                end_timestamp = start_time_stamp
                begin_timestamp = get_prev_week_timestamp(end_timestamp)
            else:
                end_timestamp = begin_timestamp
                begin_timestamp = get_prev_week_timestamp(end_timestamp)

            avg_size = 0
            avg_price = 0
            sum_size = 0
            sum_rent = 0
            num_rentals_size = 0
            num_rentals_price = 0

            print ('Search in window: ', begin_timestamp, ' and ', end_timestamp)

            if bed < 5:
                all_matching_rentals = RentalCollection.find(
                    {"Zipcode": {"$in": zips}, "Bedrooms": str(bed), "Timestamp": {"$lt": end_timestamp, "$gt": begin_timestamp}})
            else:
                all_matching_rentals = RentalCollection.find(
                    {"Zipcode": {"$in": zips}, "Bedrooms": {"$gte": str(bed)}, "Timestamp": {"$lt": end_timestamp, "$gt": begin_timestamp}})

            if all_matching_rentals.count() == 0:
                print ('no record was found, next loop')
            else:

                for matching_rental in all_matching_rentals:

                    # if we see a bad sample data then we skip both price and size calculation
                    isgooddata = True

                    p = matching_rental.get("Price")
                    # we have bad data in the database where p = 0 as an int instead of a string this might be the case
                    # where we didn't find a valid price number
                    if p != 0:
                        try:
                            # only accept rent less than $10,000/m - there are some invalid ads about selling houses
                            # instead of renting, currently no good way to filter those
                            if int(p) > 10000:
                                # print ('found big p: ', p)
                                isgooddata = False

                            if p.isdigit() and isgooddata:
                                # if int(p) > 5000:
                                #    print('large p found : ', p);
                                num_rentals_price += 1
                                sum_rent += int(p)
                        except ValueError:
                            print('bad price: ', p)

                    s = matching_rental.get("Size")
                    # we have bad data in the database where s = 0 as an int instead of a string this might be the case
                    # where we didn't find a valid size number
                    if s != 0 and isgooddata:
                        try:
                            if s.isdigit():
                                num_rentals_size += 1
                                sum_size += int(s)
                        except ValueError:
                            print ('bad size ', s)

                if (num_rentals_size > 0):
                    avg_size = int(sum_size / num_rentals_size)
                # else:
                #    avg_size = 0

                if (num_rentals_price > 0):
                    avg_price = int(sum_rent / num_rentals_price)
                # else:
                #    avg_price = 0

                print('Working on number of beds? ', bed)
                print('average size: ', avg_size)
                print('average price: ', avg_price)
                print('total good size count : ', num_rentals_size)
                print('total good price count: ', num_rentals_price)

            f.write('"' + begin_timestamp + '-' + end_timestamp + '":')
            f.write(str(avg_price) + ',')

    f.seek(-1, 1)
    f.write('}')
    f.close()
    print('Done writing file, closing :', outfn)




# this outs for the same week, 0-5 beds, and then the next week
def rent_summary_json_by_week_bed_s3(city, start_day, num_week, outfn, s3bucket):
    print ('open cstream for s3 file content')
    output = cStringIO.StringIO()
    output.write('{"weekly":' + str(num_week) + ',')

    # these are mongodb config - DO NOT CHANGE
    MyMongoClient = MongoClient("mongodb://tester:tester@ds051843.mongolab.com:51843/craiglist")
    MyMongoDB = MyMongoClient.craiglist
    RentalCollection = MyMongoDB.rental_new

    zips = supported_cities[city]
    print ('target zip list is ', zips)

    # the start date should like year:month:date
    start_time_stamp = start_day['year'] + start_day['month'] + start_day['day'] + '235959'
    begin_timestamp = 0

    # NOTE: end_timestamp is newer than begin_timestamp, we're going back in time
    for i in range(0, num_week, 1):

        if i == 0:
            end_timestamp = start_time_stamp
            begin_timestamp = get_prev_week_timestamp(end_timestamp)
        else:
            end_timestamp = begin_timestamp
            begin_timestamp = get_prev_week_timestamp(end_timestamp)

        for bed in range(0, 6, 1):

            avg_size = 0
            avg_price = 0
            sum_size = 0
            sum_rent = 0
            num_rentals_size = 0
            num_rentals_price = 0

            # print ('Search in window: ', begin_timestamp, ' and ', end_timestamp)

            if bed < 5:
                all_matching_rentals = RentalCollection.find(
                    {"Zipcode": {"$in": zips}, "Bedrooms": str(bed), "Timestamp": {"$lt": end_timestamp, "$gt": begin_timestamp}})
            else:
                all_matching_rentals = RentalCollection.find(
                    {"Zipcode": {"$in": zips}, "Bedrooms": {"$gte": str(bed)}, "Timestamp": {"$lt": end_timestamp, "$gt": begin_timestamp}})

            if all_matching_rentals.count() == 0:
                print ('no record was found, next loop')
            else:

                for matching_rental in all_matching_rentals:

                    # if we see a bad sample data then we skip both price and size calculation
                    isgooddata = True

                    p = matching_rental.get("Price")
                    # we have bad data in the database where p = 0 as an int instead of a string this might be the case
                    # where we didn't find a valid price number
                    if p != 0:
                        try:
                            # only accept rent less than $10,000/m - there are some invalid ads about selling houses
                            # instead of renting, currently no good way to filter those
                            if int(p) > 10000:
                                # print ('found big p: ', p)
                                isgooddata = False

                            if p.isdigit() and isgooddata:
                                # if int(p) > 5000:
                                #    print('large p found : ', p);
                                num_rentals_price += 1
                                sum_rent += int(p)
                        except ValueError:
                            print('bad price: ', p)

                    s = matching_rental.get("Size")
                    # we have bad data in the database where s = 0 as an int instead of a string this might be the case
                    # where we didn't find a valid size number
                    if s != 0 and isgooddata:
                        try:
                            if s.isdigit():
                                num_rentals_size += 1
                                sum_size += int(s)
                        except ValueError:
                            print ('bad size ', s)

                if (num_rentals_size > 0):
                    avg_size = int(sum_size / num_rentals_size)
                # else:
                #    avg_size = 0

                if (num_rentals_price > 0):
                    avg_price = int(sum_rent / num_rentals_price)
                # else:
                #    avg_price = 0

                # print('Working on number of beds? ', bed)
                # print('average size: ', avg_size)
                # print('average price: ', avg_price)
                # print('total good size count : ', num_rentals_size)
                # print('total good price count: ', num_rentals_price)

            # f.write('"' + begin_timestamp + '-' + end_timestamp + '":')
            # f.write(str(avg_price) + ',')
            print >> output, '"' + begin_timestamp + '-' + end_timestamp + '":'
            print >> output, str(avg_price) + ','

    # need to remove the last ',' and the call above adds a \n char, so we remove the last 2 chars
    f1 = output.getvalue()[:-2]
    f2 = f1 + '}'
    output.close()
    s3interface.upload_file_to_s3(f2, outfn, s3bucket)








# pass in the output json file name, how many weeks we go back, and the starting day, we generate json file contains
# the following:
# "weekly":8,
# "2016/07/15-2016/07/22":888, (studio rent)
# "1br_2016/07/15-2016/07/22":1671, (1br rent)
# city is the city name, like seattle, bellevue, see zipcodes.py file
# outfn is the file name
# num_weeks is the number of weeks we go back
# start_day is the beginning date going back
def write_rent_summary_json_by_week_beds(city, start_day, num_week, outfn):
    print('open file for writing : ', outfn)
    f = open(outfn, 'w')
    f.write('{"weekly":' + str(num_week) + ',')

    # these are mongodb config - DO NOT CHANGE
    MyMongoClient = MongoClient("mongodb://tester:tester@ds051843.mongolab.com:51843/craiglist")
    MyMongoDB = MyMongoClient.craiglist
    RentalCollection = MyMongoDB.rental_new

    zips = supported_cities[city]
    print ('target zip list is ', zips)

    # the start date should like year:month:date
    start_time_stamp = start_day['year'] + start_day['month'] + start_day['day'] + '235959'
    begin_timestamp = 0

    # NOTE: end_timestamp is newer than begin_timestamp, we're going back in time
    for i in range(0, num_week, 1):

        if i == 0:
            end_timestamp = start_time_stamp
            begin_timestamp = get_prev_week_timestamp(end_timestamp)
        else:
            end_timestamp = begin_timestamp
            begin_timestamp = get_prev_week_timestamp(end_timestamp)

        for bed in range(0, 6, 1):

            avg_size = 0
            avg_price = 0
            sum_size = 0
            sum_rent = 0
            num_rentals_size = 0
            num_rentals_price = 0

            print ('Search in window: ', begin_timestamp, ' and ', end_timestamp)

            if bed < 5:
                all_matching_rentals = RentalCollection.find(
                    {"Zipcode": {"$in": zips}, "Bedrooms": str(bed), "Timestamp": {"$lt": end_timestamp, "$gt": begin_timestamp}})
            else:
                all_matching_rentals = RentalCollection.find(
                    {"Zipcode": {"$in": zips}, "Bedrooms": {"$gte": str(bed)}, "Timestamp": {"$lt": end_timestamp, "$gt": begin_timestamp}})

            if all_matching_rentals.count() == 0:
                print ('no record was found, next loop')
            else:

                for matching_rental in all_matching_rentals:

                    # if we see a bad sample data then we skip both price and size calculation
                    isgooddata = True

                    p = matching_rental.get("Price")
                    # we have bad data in the database where p = 0 as an int instead of a string this might be the case
                    # where we didn't find a valid price number
                    if p != 0:
                        try:
                            # only accept rent less than $10,000/m - there are some invalid ads about selling houses
                            # instead of renting, currently no good way to filter those
                            if int(p) > 10000:
                                # print ('found big p: ', p)
                                isgooddata = False

                            if p.isdigit() and isgooddata:
                                # if int(p) > 5000:
                                #    print('large p found : ', p);
                                num_rentals_price += 1
                                sum_rent += int(p)
                        except ValueError:
                            print('bad price: ', p)

                    s = matching_rental.get("Size")
                    # we have bad data in the database where s = 0 as an int instead of a string this might be the case
                    # where we didn't find a valid size number
                    if s != 0 and isgooddata:
                        try:
                            if s.isdigit():
                                num_rentals_size += 1
                                sum_size += int(s)
                        except ValueError:
                            print ('bad size ', s)

                if (num_rentals_size > 0):
                    avg_size = int(sum_size / num_rentals_size)
                # else:
                #    avg_size = 0

                if (num_rentals_price > 0):
                    avg_price = int(sum_rent / num_rentals_price)
                # else:
                #    avg_price = 0

                print('Working on number of beds? ', bed)
                print('average size: ', avg_size)
                print('average price: ', avg_price)
                print('total good size count : ', num_rentals_size)
                print('total good price count: ', num_rentals_price)

            f.write('"' + begin_timestamp + '-' + end_timestamp + '":')
            f.write(str(avg_price) + ',')

    f.seek(-1, 1)
    f.write('}')
    f.close()
    print('Done writing file, closing :', outfn)

def entry_point():
    now = datetime.datetime.now()
    if now.month < 10:
        t_m = '0' + str(now.month)
    else:
        t_m = str(now.month)
    if now.day < 10:
        t_d = '0' + str(now.day)
    else:
        t_d = str(now.day)


    test_start_day = {'year': str(now.year), 'month': t_m, 'day': t_d}

    print ('today is ', test_start_day)

    rent_summary_json_by_bed_week_s3('seattle', test_start_day, 8, 'json/new_rent_data/seattle_rent_weekly.json', 'dreamhomereport.com')
    rent_summary_json_by_bed_week_s3('bellevue', test_start_day, 8, 'json/new_rent_data/bellevue_rent_weekly.json', 'dreamhomereport.com')
    rent_summary_json_by_bed_week_s3('kirkland', test_start_day, 8, 'json/new_rent_data/kirkland_rent_weekly.json', 'dreamhomereport.com')
    rent_summary_json_by_bed_week_s3('redmond', test_start_day, 8, 'json/new_rent_data/redmond_rent_weekly.json', 'dreamhomereport.com')
    rent_summary_json_by_bed_week_s3('issaquah', test_start_day, 8, 'json/new_rent_data/issaquah_rent_weekly.json', 'dreamhomereport.com')
    rent_summary_json_by_bed_week_s3('medina', test_start_day, 8, 'json/new_rent_data/medina_rent_weekly.json', 'dreamhomereport.com')
    rent_summary_json_by_bed_week_s3('sammamish', test_start_day, 8, 'json/new_rent_data/sammamish_rent_weekly.json', 'dreamhomereport.com')
    rent_summary_json_by_bed_week_s3('mercer_island', test_start_day, 8, 'json/new_rent_data/mercer_island_rent_weekly.json', 'dreamhomereport.com')



def test():
    timestamp = '20170209123456'
    year = timestamp[0:4]
    month = timestamp[4:6]
    day = timestamp[6:8]
    print ('year : ', year)
    print ('month : ', month)
    print ('day : ', day)


def main():
    entry_point()

if __name__ == "__main__":
    main()
